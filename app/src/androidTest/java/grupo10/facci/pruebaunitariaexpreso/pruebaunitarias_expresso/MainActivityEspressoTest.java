package grupo10.facci.pruebaunitariaexpreso.pruebaunitarias_expresso;

import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.security.PublicKey;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)

public class MainActivityEspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void asegurarTextoCambiado (){
        onView(withId(R.id.editTextCambio))
                .perform(typeText("Hola"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.buttonCambiar)).perform(click());
        onView(withId(R.id.editTextCambio)).check(matches(withText("Hola")));
    }

    @Test
    public void asegurarReset () {
        onView(withId(R.id.editTextCambio))
                .perform(typeText("Hola"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.buttonReset)).perform(click());
        onView(withId(R.id.editTextCambio)).check(matches(withText("Texto")));
    }

    @Test
    public void asegurarCambioActivity (){
        onView(withId(R.id.editTextCambio))
                .perform(typeText("Segunda Activity"), ViewActions.closeSoftKeyboard());
        onView(withId(R.id.buttonCambiarAct)).perform(click());
        onView(withId(R.id.textViewMuestra)).check(matches(withText("Segunda Activity")));
    }

}
