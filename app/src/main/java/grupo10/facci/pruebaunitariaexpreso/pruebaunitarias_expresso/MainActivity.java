package grupo10.facci.pruebaunitariaexpreso.pruebaunitarias_expresso;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText editTextCambio;
    TextView textViewTexto;
    Button buttonCambiarAct, buttonCambiar, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextCambio = (EditText) findViewById(R.id.editTextCambio);
        textViewTexto = (TextView) findViewById(R.id.textViewTexto);
        buttonCambiarAct = (Button) findViewById(R.id.buttonCambiarAct);
        buttonCambiar = (Button) findViewById(R.id.buttonCambiar);
        buttonReset = (Button) findViewById(R.id.buttonReset);

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextCambio.setText("Texto");
            }
        });

        buttonCambiarAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                intent.putExtra("input", editTextCambio.getText().toString());
                startActivity(intent);
            }
        });

        buttonCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewTexto.setText(editTextCambio.getText().toString());
            }
        });
    }
}
