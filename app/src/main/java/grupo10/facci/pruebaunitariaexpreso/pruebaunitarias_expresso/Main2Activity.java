package grupo10.facci.pruebaunitariaexpreso.pruebaunitarias_expresso;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView textViewMuestra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textViewMuestra = (TextView) findViewById(R.id.textViewMuestra);
        Bundle inputData = getIntent().getExtras();
        String input = inputData.getString("input");
        textViewMuestra.setText(input);

    }
}
